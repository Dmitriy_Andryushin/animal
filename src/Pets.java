/**
 * Абстрактный класс для наследования животным
 */
public abstract class Pets {
    /**
     * Имя животного
     */
    private String name;
    /**
     * Сытость животного
     */
    private int satiety;
    /**
     * Усталость животного
     */
    private int fatigue;

    /**
     * Конструктор для животных
     */
    public Pets(String name, int satiety, int fatigue) {
        this.name = name;
        this.satiety = satiety;
        this.fatigue = fatigue;
    }

    /**
     * Абстрактный метод игры
     */
    abstract void play();

    /**
     * Абстрактный метод сна
     */
    abstract void sleep();

    /**
     * Абстрактный метод кормления
     */
    abstract void eat();

    void setFatigue(int fatigue) {
        this.fatigue = fatigue;
    }

    int getFatigue() {
        return fatigue;
    }

    void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    int getSatiety() {
        return satiety;
    }

    String getName() {
        return name;
    }

}
