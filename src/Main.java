public class Main {
    public static final String PET_FEEDING = "Покормил питомца";
    public static final String SLEEPING_PET = "Уложил спать питомца";
    public static final String PLAYS_A_PET = "Поиграл с питомцем";

    public static void main(String[] args) {
        Cat cat = new Cat("Кот", 63, 70);
        Dog dog = new Dog("Собака", 70, 64);
        Hamster hamster = new Hamster("Хомяк", 75, 50);
        Pets[] animals = new Pets[]{cat, dog, hamster};

        for (int x = 8; x <= 22; x++) {
            System.out.println("Время " + x + ":00");
            for (int y = 0; y < 3; y++) {
                if (animals[y].getSatiety() < 25) {
                    animals[y].eat();
                    System.out.println(animals[y].getName() + " " + PET_FEEDING);
                } else if (animals[y].getFatigue() < 25) {
                    animals[y].sleep();
                    System.out.println(animals[y].getName() + " " + SLEEPING_PET);
                } else {
                    animals[y].play();
                    System.out.println(animals[y].getName() + " " + PLAYS_A_PET);
                }


            }
        }
        mostHungry(animals);
    }

    /**
     * Метод вычисляющий самого голодного животного
     */
    public static void mostHungry(Pets[] animals) {
        if (animals[0].getSatiety() < animals[1].getSatiety() && animals[0].getSatiety() < animals[2].getSatiety()) {
            System.out.println("Самый голодный " + animals[0].toString());
        }
        if (animals[1].getSatiety() < animals[0].getSatiety() && animals[1].getSatiety() < animals[2].getSatiety()) {
            System.out.println("Самый голодный " + animals[1].toString());
        }
        if (animals[2].getSatiety() < animals[1].getSatiety() && animals[2].getSatiety() < animals[0].getSatiety()) {
            System.out.println("Самый голодный " + animals[2].toString());
        }
    }
}